import { Component, } from '@angular/core';
import { v4 as uuidv4 } from 'uuid'; // import de la bibliothèque uuid
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import { Router } from '@angular/router';

/* stockent les informations sur les interventions chirurgicales  */
export interface Intervention {
  id: string;
  surgeon: string;
  specialty: string;
  anesthesist: string;
  nurse1: string;
  nurse2: string;
  roomNumber: string;
  intervention: string;
}
/* stockent les informations sur les statistiques des chirurgiens  */
export interface SurgeonStats {
  id: string;
  name: string;
  specialty: string;
  roomNumber: string;
  numInterventions: number;
  mostFrequentSpecialty: string;
  mostFrequentNurse: string;
  mostFrequentAnesthesist:string
}

@Component({
  selector: 'app-interventions',
  templateUrl: './interventions.component.html',
  styleUrls: ['./interventions.component.css']
})
export class InterventionsComponent {
  interventions: Intervention[] = [];
  surgeonStats: SurgeonStats[] = [];
  currentPage: number = 1;
  RESULTS_PER_PAGE: number = 10;

  constructor(private csvParser: NgxCsvParser, private router: Router) {
    const interventions = localStorage.getItem('interventions');
    if (interventions) {
      this.interventions = JSON.parse(interventions);
      this.computeSurgeonStats();
    }
  }
  /* La méthode readCSV est appelée lorsqu'un utilisateur télécharge un fichier CSV. 
  Il utilise le parseur CSV pour lire le fichier et stocker les données dans la variable 
  d'interventions de la classe.Ensuite, il appelle la méthode computeSurgeonStats pour calculer 
  les statistiques des chirurgiens et stocker les interventions dans le stockage local. */

  readCSV(event: any) {
    const files = event.target.files;
    this.csvParser.parse(files[0], { header: true, delimiter: ';' }).pipe().subscribe((result: any) => {
      this.interventions = result.map((row: any) => ({
        surgeon: row['surgeon'],
        specialty: row['specialty'],
        anesthesist: row['anesthesiste'],
        nurse1: row['nurse1'],
        nurse2: row['nurse2'],
        roomNumber: row['roomNumber'],
        intervention: row['intervention'],
      }));
  
      this.computeSurgeonStats();
  
      // Save the interventions to localStorage
      localStorage.setItem('interventions', JSON.stringify(this.interventions));
    });
  }
  /* calcule l'infirmiere le plus fréquente pour un chirurgien donné. */
  getMostFrequentNurse(surgeonName: string): string {
    const nurses = this.interventions.filter(i => i.surgeon === surgeonName).map(i => [i.nurse1, i.nurse2]).flat();
    const nurseCounts = new Map<string, number>();
    for (const nurse of nurses) {
      const count = nurseCounts.get(nurse) ?? 0;
      nurseCounts.set(nurse, count + 1);
    }
    let mostFrequentNurse = '';
    let maxCount = 0;
    for (const [nurse, count] of nurseCounts) {
      if (count > maxCount) {
        mostFrequentNurse = nurse;
        maxCount = count;
      }
    }

    return mostFrequentNurse;
  }


  /* calcule l'anesthsiste le plus fréquent pour un chirurgien donné. */
  getMostFrequentAnesthesist(surgeonName: string): string {
    const interventions = this.interventions.filter(i => i.surgeon === surgeonName);
    const anesthesists = interventions.map(i => i.anesthesist);
    const anesthesistCounts = new Map<string, number>();
    for (const anesthesist of anesthesists) {
      const count = anesthesistCounts.get(anesthesist) ?? 0;
      anesthesistCounts.set(anesthesist, count + 1);
    }
    let mostFrequentAnesthesist = '';
    let maxCount = 0;
    for (const [anesthesist, count] of anesthesistCounts) {
      if (count > maxCount) {
        mostFrequentAnesthesist = anesthesist;
        maxCount = count;
      }
    }
    return mostFrequentAnesthesist;
  }
  
/*  La fonction computeSurgeonStats calcule les statistiques pour chaque chirurgien à partir d'une liste d'interventions et stocke les résultats dans un tableau surgeonStats.
Elle utilise une Map pour stocker les statistiques de chaque chirurgien, et pour chaque intervention, elle récupère le nom du chirurgien et le numéro de salle.
Si le nom du chirurgien est déjà dans la Map, elle met à jour ses statistiques. Sinon, elle crée un nouvel objet SurgeonStats, l'ajoute à la Map et initialise ses valeurs avec 
les informations de l'intervention.Elle incrémente également le nombre d'interventions pour chaque chirurgien, et si le numéro de salle est différent de celui stocké dans les 
statistiques du chirurgien, elle compte le nombre d'interventions effectuées par le chirurgien dans cette salle et met à jour les statistiques si nécessaire.
Ensuite, elle parcourt toutes les interventions effectuées par le chirurgien et stocke le nombre de fois où chaque spécialité a été pratiquée dans une autre Map. 
Elle met également à jour la spécialité la plus fréquente, ainsi que les noms des infirmières et des anesthésistes les plus fréquemment travaillés avec */
  computeSurgeonStats() {
    const surgeonMap = new Map<string, SurgeonStats>();
  
    for (const intervention of this.interventions) {
      const surgeonName = intervention.surgeon;
      const roomNumber = intervention.roomNumber;
  
      let surgeonStats: SurgeonStats;
      if (surgeonMap.has(surgeonName)) {
        surgeonStats = surgeonMap.get(surgeonName)!;
      } else {
        surgeonStats = {
          id: uuidv4(), // génération d'un identifiant unique
          name: surgeonName,
          specialty: intervention.specialty,
          roomNumber: '',
          numInterventions: 0,
          mostFrequentSpecialty: '',
          mostFrequentNurse: '',
          mostFrequentAnesthesist:''
        };
        surgeonMap.set(surgeonName, surgeonStats);
      }
  
      surgeonStats.numInterventions++;
  
      if (roomNumber !== surgeonStats.roomNumber || surgeonStats.roomNumber === '') {
        const count = this.interventions.filter(i => i.surgeon === surgeonName && i.roomNumber === roomNumber).length;
        if (count > surgeonStats.numInterventions) {
          surgeonStats.numInterventions = count;
          surgeonStats.roomNumber = roomNumber;
        }
      }
  
      const specialtyCounts = new Map<string, number>();
      for (const intervention of this.interventions.filter(i => i.surgeon === surgeonName)) {
        const specialty = intervention.specialty;
        const count = specialtyCounts.get(specialty) ?? 0;
        specialtyCounts.set(specialty, count + 1);
        if (count + 1 > (specialtyCounts.get(surgeonStats.mostFrequentSpecialty) ?? 0)) {
          surgeonStats.mostFrequentSpecialty = specialty;
        }
        const mostFrequentNurse = this.getMostFrequentNurse(surgeonName);
        surgeonStats.mostFrequentNurse = mostFrequentNurse;
        const mostFrequentAnesthesist = this.getMostFrequentAnesthesist(surgeonName);
        surgeonStats.mostFrequentAnesthesist = mostFrequentAnesthesist;
      }
  
      this.surgeonStats = Array.from(surgeonMap.values());
    }
  }
/* détermine quelle page de résultats afficher */
  getSurgeonStatsForPage() {
    const start = (this.currentPage - 1) * this.RESULTS_PER_PAGE;
    const end = start + this.RESULTS_PER_PAGE;
    return this.surgeonStats.slice(start, end);
  }
  
  prevPage() {
    this.currentPage--;
  }
  
  nextPage() {
    this.currentPage++;
  }



  
  

  goToSurgeonDetails(surgeon: SurgeonStats) {
    const surgeonData = JSON.stringify(surgeon);
    this.router.navigate(['/surgeonDetails'], { queryParams: { surgeon: surgeonData } });
  }
  
}  