import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SurgeonStats } from '../interventions/interventions.component';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
  surgeon: SurgeonStats | undefined;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params['surgeon']) { // Utilisez la clé 'surgeon' pour récupérer les informations sur le chirurgien
        this.surgeon = JSON.parse(params['surgeon']) as SurgeonStats; // ajuster le type de données attendu
      }
    });
  }
}
