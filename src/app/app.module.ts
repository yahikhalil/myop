import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { InformationComponent } from './information/information.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InterventionsComponent } from './interventions/interventions.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', redirectTo: 'surgeon', pathMatch: 'full' },
  { path: 'surgeon', component: InterventionsComponent },
  { path: 'surgeonDetails', component: InformationComponent },
  { path: '**', redirectTo: 'surgeon', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    InformationComponent,
    NavbarComponent,
    InterventionsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
